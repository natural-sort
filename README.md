## About

A CHICKEN implementation of natural sorting as described on [Coding
Horror].

## Docs

See [its wiki page].

[Coding Horror]: https://blog.codinghorror.com/sorting-for-humans-natural-sort-order/
[its wiki page]: http://wiki.call-cc.org/eggref/5/natural-sort
